package org.example;

public class Main {

    public static void hello() {

        System.out.println("Hello World!");
    }

    public static void hello2() {
        System.out.println("Hello World!");
    }

    public static void hello3() {
        System.out.println("Hello World!");
    }

    public static void main(String[] args) {
        hello();
        hello2();
        hello3();
    }

    public static void helloWorld() {

        System.out.println("Hello World!!!");
    }
}
